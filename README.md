# IRD SS 24

## Project Setup

### Datenbank
Führe das Skript [datenbank-installation.sql](./database/datenbank-installation.sql) via PGAdmin aus.

### Frontend
Öffne neuen Terminal und führe folgende Befehle aus:
- npm install
- npm run dev

### Backend (rest api)
Öffne neuen Terminal und führe folgende Befehle aus:
- cd .\rest-api\
- npm install
- npm run start (server hoch fahren)
- strg + c (server abschalten)