-- Install PostGIS
create extension if not exists postgis;

-- Sight Tabell
drop table if exists sights;
create table sights (
	id serial primary key,
	name text not null,
	type text not null,
	price numeric null,
	note text null,
	geometry geometry
);

insert into sights (name, type, price, note, geometry)
values ('Fernsehturm', 'others', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(48.797, 9.172));

insert into sights (name, type, price, note, geometry)
values ('Schloss Neuschwanstein', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(48.786, 9.252));

insert into sights (name, type, price, note, geometry)
values ('Deutsches Museum', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(48.777, 9.166));

insert into sights (name, type, price, note, geometry)
values ('Mercedes-Benz Museum', 'history', 7, 'Meine erste Sehenswürdigkeit', ST_MakePoint(48.785, 9.199));
