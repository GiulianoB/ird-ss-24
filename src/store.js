import { reactive } from "vue";

export const store = reactive({
  sights: [],
  addSight(sight) {
    this.sights.push(sight);
  },
});
