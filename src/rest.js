import { store } from "./store";

export async function getSights() {
    const response = await fetch('http://localhost:4005/sights?format=geojson', {
        method: 'GET'
    })
    const responseBody = await response.json()
    const geojson = responseBody[0].geojson

    for (let i = 0; i < geojson.features.length; i++) {
        store.addSight({
            id: geojson.features[i].properties.id,
            name: geojson.features[i].properties.name,
            type: geojson.features[i].properties.type,
            lat: geojson.features[i].geometry.coordinates[0],
            lng: geojson.features[i].geometry.coordinates[1]
        })
    }
}

export async function postSight(sight) {
    const response = await fetch('http://localhost:4005/sights', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
        body: JSON.stringify(sight)
    })
}